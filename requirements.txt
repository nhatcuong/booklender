Flask==0.11
Flask-SQLAlchemy==1.0
requests==2.8.0
Django==1.10
djangorestframework==3.4.3
nose==1.3.7